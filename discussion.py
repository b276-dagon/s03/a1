

names = ["John", "George", "Ringo"] #String list
programs = ["Developer career", "pi-shape", "short courses"] #String list

truth_variables = [True, False, True, True, False] #boolean list

# A list can contain emelents with different data types:
sample_list = ["Apple", 3, False, "Potato", 4, False]

# Note: Problem may arise if you try to use lists with multiple type for something that expexcrs the, to be all the same type

#Getting the lists
print(len(programs))
print(names[0])
print(names[-1])
print(programs[2])

# Add or push in LIST
programs.append('global')
print(programs)
# Deleting List Items
duration = [260, 180, 20] #number list
duration.append(360)
print(duration)

del duration[-1]
print(duration)

# Membership checking - the "in" keyword check if the elemts is in the lists
# return True or False value
print(20 in duration) #True value
print(500 in duration)# False value

# Sorting lists- The sort() method sorts the list alphanumerically, ascending, by default
duration.sort()
print(duration)

programs.sort()
print(programs)

# emptying the list - the clear() method is used to empty the contents of the list

test_list = [1, 3, 4, 5, 6]
print(test_list)

test_list.clear()
print(test_list)





mult = lambda a,b : a * b

print(mult(5,6))


